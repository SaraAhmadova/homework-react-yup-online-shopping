let initialState = [];

export const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case "GET_CART_PRODUCTS": {
      return action.payload;
    }
    default:
      return state;
  }
};

try {
  const data = localStorage.getItem("cart");
  initialState = data ? JSON.parse(data) : [];
} catch (err) {
  console.error("Error parsing cart data:", err);
  initialState = [];
}
