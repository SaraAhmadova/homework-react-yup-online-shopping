import { productsReducer } from "./productsReducer";
import { combineReducers } from "redux";
import { modalReducer } from "./modalReducer";
import { cartReducer } from "./cartReducer";
import { favoritesReducer } from "./favoritesReducer";
import { purchaseReducer } from './purchaseReucer';

export const rootReducer=combineReducers({
    modal:modalReducer,
    products:productsReducer,
    cart:cartReducer,
    favorites:favoritesReducer,
    purchase: purchaseReducer
})