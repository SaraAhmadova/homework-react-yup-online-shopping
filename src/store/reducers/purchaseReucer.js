const initialState = {
    purchaseProducts: [],
    isOpen: false,
  };
  
  export const purchaseReducer = (state = initialState, action) => {
    switch (action.type) {
      case "OPEN_PURCHASE_MODAL":
        return { ...action.payload };
      case "CLOSE_PURCHASE_MODAL":
        return { ...action.payload };
  
      default:
        return state;
    }
  };
  
  