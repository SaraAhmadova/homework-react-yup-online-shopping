export const openModalAction = (product) => (dispatch, getState) => {
  dispatch({
    type: "OPEN_MODAL",
    payload: { modalProduct: { ...product }, isOpen: true },
  });
};

export const closeModalAction = (dispatch, getState) => {
  dispatch({
    type: "CLOSE_MODAL",
    payload: { modalProduct: {}, isOpen: false },
  });
};

