export const favoriteProducts = (product) => (dispatch, getState) => {
  const favorites = getState().favorites; 

  if (favorites.includes(product)) {
    const filteredFavorites = favorites.filter(
      (favorite) => favorite.sku !== product.sku
    );
    dispatch({
      type: "GET_FAVORITES_PRODUCTS",
      payload: filteredFavorites,
    });
  } else {
    dispatch({
      type: "GET_FAVORITES_PRODUCTS",
      payload: [...favorites, product],
    });
  }
};
