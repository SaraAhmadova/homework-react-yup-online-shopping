export const openPurchaseModalAction = (dispatch, getState) => {
  const cartProducts = getState().cart;
  dispatch({
    type: "OPEN_PURCHASE_MODAL",
    payload: { purchaseProducts: [...cartProducts], isOpen: true },
  });
};

export const closePurchaseModalAction = (dispatch, getState) => {
  const purchase = getState().purchase;
  dispatch({
    type: "CLOSE_PURCHASE_MODAL",
    payload: { ...purchase, isOpen: false },
  });
};
