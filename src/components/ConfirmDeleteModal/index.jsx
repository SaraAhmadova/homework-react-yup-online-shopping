import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeModalAction } from "../../store/actions/modal";
import { removeCart } from "../../store/actions/cart";
import "./ConfirmDeleteModal.css";

const ConfirmDeleteModal = () => {
  const modalState = useSelector((state) => state.modal);
  const dispatch = useDispatch();
  const onClose = () => dispatch(closeModalAction);
  const onSubmitDeleteProduct = () => {
    dispatch(removeCart(modalState.modalProduct));
    dispatch(closeModalAction);
  };
  return (
    <div className="cart-modal" onClick={onClose}>
      <div className="cart-modal-content" onClick={(e) => e.stopPropagation()}>
        <span className="cart-modal-close" onClick={onClose}>
          &times;
        </span>
        <h2>Are you sure to delete product from basket?</h2>
        <button onClick={onClose}>Cancel</button>
        <button onClick={onSubmitDeleteProduct}>Yes</button>
      </div>
    </div>
  );
};

export default ConfirmDeleteModal;
