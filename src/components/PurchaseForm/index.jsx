import React from "react";
import "./PurchaseForm.css";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import { closePurchaseModalAction } from "../../store/actions/purchase";

const PurchaseForm = () => {
  const dispatch = useDispatch();
  const initialValues = {
    firstName: "",
    lastName: "",
    age: "",
    address: "",
    phoneNumber: "",
  };
  const purchaseSchema = yup.object().shape({
    firstName: yup.string().required("Required!"),
    lastName: yup.string().required("Required!"),
    age: yup
      .number()
      .min(18, "Age must be older than 18!")
      .required("Required!"),
    address: yup.string().required("Required!"),
    phoneNumber: yup
      .string()
      .matches(
        /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g,
        "Phone number is not written in righ format!"
      )
      .required("Required!"),
  });

  const cartProducts = useSelector((state) => state.cart);
  const handleSubmit = (values, { resetForm }) => {
    console.log(values);

    localStorage.removeItem("cart");
    localStorage.setItem("checkout", JSON.stringify(cartProducts));
    dispatch(closePurchaseModalAction)
    resetForm();
  };
  const onClose = ()=>{
    dispatch(closePurchaseModalAction)
  }
  return (
    <div className="cart-modal" onClick={onClose}>
      <div className="cart-modal-content" onClick={(e) => e.stopPropagation()}>
        <span className="cart-modal-close" onClick={onClose}>
          &times;
        </span>
          <h2>User Information Form</h2>
          <Formik
            initialValues={initialValues}
            validationSchema={purchaseSchema}
            onSubmit={handleSubmit}
          >
            <Form className="user-form">
              <div className="form-group">
                <label htmlFor="firstName">First Name</label>
                <Field type="text" id="firstName" name="firstName" />
                <ErrorMessage
                  name="firstName"
                  component="div"
                  className="error-message"
                />
              </div>

              <div className="form-group">
                <label htmlFor="lastName">Last Name</label>
                <Field type="text" id="lastName" name="lastName" />
                <ErrorMessage
                  name="lastName"
                  component="div"
                  className="error-message"
                />
              </div>

              <div className="form-group">
                <label htmlFor="age">Age</label>
                <Field type="text" id="age" name="age" />
                <ErrorMessage
                  name="age"
                  component="div"
                  className="error-message"
                />
              </div>

              <div className="form-group">
                <label htmlFor="address">Delivery Address</label>
                <Field type="text" id="address" name="address" />
                <ErrorMessage
                  name="address"
                  component="div"
                  className="error-message"
                />
              </div>

              <div className="form-group">
                <label htmlFor="phoneNumber">Mobile Phone Number</label>
                <Field type="text" id="phoneNumber" name="phoneNumber" />
                <ErrorMessage
                  name="phoneNumber"
                  component="div"
                  className="error-message"
                />
              </div>

              <button type="submit">Submit</button>
            </Form>
          </Formik>
      </div>
    </div>
  );
};

export default PurchaseForm;
