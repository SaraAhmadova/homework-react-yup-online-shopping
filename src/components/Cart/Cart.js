import React from "react";

const Cart = ({ onClick, cartCount }) => {
  return (
    <div className="cart" onClick={onClick}>
      <span role="img" aria-label="cart">
        🛒
      </span>
      {cartCount > 0 && <span className="cart-count">{cartCount}</span>}
    </div>
  );
};

export default Cart;
