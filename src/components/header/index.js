import React from "react";
import "./header.css";
import { Link } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { openPurchaseModalAction } from "../../store/actions/purchase";

export const Header = (props) => {
  const dispatch = useDispatch();
  const openPurchaseModal = ()=>{
    dispatch(openPurchaseModalAction)
  }
  return (
    <ul className={"list-container"}>
      <li className={"list-item"}>
        <button>
          <Link to={"/"}>
            <span>Home</span>
          </Link>
        </button>
      </li>
      <li className={"list-item"}>
        <button>
          <Link to={"/favorites"}>
            <span role="img" aria-label="favorites">
              ⭐
            </span>
            <span>Favorites</span>
          </Link>
        </button>
      </li>
      <li className={"list-item"}>
        <button>
          <Link to={"/cart"}>
            <span role="img" aria-label="cart">
              🛒
            </span>
            <span>Cart</span>
          </Link>
        </button>
      </li>
     {props.isCartPage &&  <li className={"list-item"}>
        <button onClick={openPurchaseModal}>
            <span role="img" aria-label="cart">
              🛒
            </span>
            <span>Purchase</span>
        </button>
      </li>}
    </ul>
  );
};
