import React from "react";
import ProductCard from "../../components/ProductCard";
import ConfirmDeleteModal from "../../components/ConfirmDeleteModal/index.jsx";
import { Header } from "../../components/header";
import { useSelector } from "react-redux";
import PurchaseForm from "../../components/PurchaseForm";

const CartPage = () => {
  const productList = useSelector((state) => state.cart);
  const modalState = useSelector((state) => state.modal);
  const purchaseModalState = useSelector(state=> state.purchase)
  return (
    <div>
      <h1>Cart page</h1>
      <Header isCartPage={true} />
      <div className="product-list">
        {productList?.length > 0 &&
          productList.map((product, index) => (
            <ProductCard key={index} product={product} isCartProduct={true} />
          ))}
      </div>
      {purchaseModalState.isOpen && <PurchaseForm />}
      {modalState.isOpen && <ConfirmDeleteModal />}
    </div>
  );
};

export default CartPage;
