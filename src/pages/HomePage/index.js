import React from "react";
import "./HomePage.css";
import ProductList from "../../components/ProductList";
import { Header } from "../../components/header";
import { useSelector } from "react-redux";

const HomePage = () => {
  const products = useSelector((state) => state.products.products);
  const cartProducts = useSelector((state) => state.cart);
  const favProducts = useSelector((state) => state.favorites);


  return (
    <div>
      <h1>Online Store</h1>
      <Header
        favoritesCount={favProducts?.length ?? 0}
        cartCount={cartProducts?.length ?? 0}
      />
      {products && (
        <ProductList
          products={products}
          favorites={favProducts}
        />
      )}
    </div>
  );
};

export default HomePage;
