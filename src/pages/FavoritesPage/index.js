import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Header } from "../../components/header";
import ProductCard from "../../components/ProductCard";
import { favoriteProducts } from "../../store/actions/favorites";

const FavoritesPage = () => {
  const favProductList = useSelector((state) => state.favorites);
  const dispatch = useDispatch();
  const onToggleFavorite = (product) => {
    dispatch(favoriteProducts(product));
  };

  return (
    <div>
      <h1>Favorites page</h1>

      <Header />
      <div className="product-list">
        {favProductList.map((product, index) => (
          <ProductCard
            key={index}
            product={product}
            isFavorite={true}
            onToggleFavorite={() => {
              onToggleFavorite(product);
            }}
          />
        ))}
      </div>
    </div>
  );
};

export default FavoritesPage;
